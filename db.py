from neo4j import GraphDatabase, basic_auth

from config import config_db

driver = GraphDatabase.driver(config_db['host'], auth=basic_auth(user=config_db['user']['username'], password=config_db['user']['password']))

session = driver.session()
session.run("CREATE (a:Person {name:'Ann'})")
session.run("CREATE (b:Post {name:'Test Post'})")
session.run("""MATCH (a:Person),(b:Post)
               CREATE (a)-[r:Post{data:"12/12/2014",price:55000}]->(b)
               RETURN r""")

result = session.run("MATCH (a:Person) RETURN a.name AS name")
for record in result:
    print(record["name"])
session.close()