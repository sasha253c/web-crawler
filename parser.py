#!/usr/bin/env python3

import facebook
import json
from pprint import pprint

from config import facebook_access_token


kwargs = {'fields': ['email', 'link', 'birthday', 'likes', 'taggable_friends', ]}
kwargs = {'fields': ['email', 'feed', 'posts']}


graph = facebook.GraphAPI(access_token=facebook_access_token)

profile = graph.get_object(id='me', **kwargs)
friends = graph.get_connections(id='me', connection_name='friends')
print(friends)



profile_json = json.dumps(profile, indent=3, ensure_ascii=False, sort_keys=True)

pprint(profile)
with open('profile.json', 'w') as output:
    output.write(profile_json)


